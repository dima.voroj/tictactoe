import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Player player1 = new AIPlayer(Symbol.X);
        Player player2 = new AIPlayer(Symbol.O);
        Field f = new Field();
        Player activePlayer = player1;
        while (!f.win()) {
            while (!f.setSymbol(activePlayer.getSymbol(), activePlayer.getCoordinates())) {
                System.out.println("Введите новые координаты:");
            }
            System.out.println(f);
            if (activePlayer.equals(player1)) {
                activePlayer = player2;
            } else {
                activePlayer = player1;
            }
        }
    }
}

class Player {
    private final Symbol symbol;

    public Player(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public int[] getCoordinates() {
        System.out.println("Ввести координаты:");
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[2];
        arr[0] = scanner.nextInt();
        arr[1] = scanner.nextInt();

        return arr;
    }
}

class AIPlayer extends Player {

    public AIPlayer(Symbol symbol) {
        super(symbol);
    }

    @Override
    public int[] getCoordinates() {
        Random r = new Random();
        int[] arr = new int[2];
        arr[0] = r.nextInt(3);
        arr[1] = r.nextInt(3);
        return arr;
    }
}

class Field {
    private final Symbol[][] field = new Symbol[3][3];

    public boolean setSymbol(Symbol s, int... i) {
        if(field[i[0]][i[1]] == null) {
            field[i[0]][i[1]] = s;
            return true;
        }
        return false;
    }

    public boolean win() {
        if (field[0][0] != null && field[0][0] == field[1][1] && field[1][1] == field[2][2]) {
            System.out.println("Победитель: " + field[0][0]);
            return true;
        }

        if (field[0][0] != null && field[0][0] == field[0][1] && field[0][1] == field[0][2]) {
            System.out.println("Победитель: " + field[0][0]);
            return true;
        }

        if (field[1][0] != null && field[1][0] == field[1][1] && field[1][1] == field[1][2]) {
            System.out.println("Победитель: " + field[1][0]);
            return true;
        }

        if (field[2][0] != null && field[2][0] == field[2][1] && field[2][1] == field[2][2]) {
            System.out.println("Победитель: " + field[2][0]);
            return true;
        }

        if (field[0][0] != null && field[0][0] == field[1][0] && field[1][0] == field[2][0]) {
            System.out.println("Победитель: " + field[0][0]);
            return true;
        }

        if (field[0][1] != null && field[0][1] == field[1][1] && field[1][1] == field[2][1]) {
            System.out.println("Победитель: " + field[0][1]);
            return true;
        }

        if (field[0][2] != null && field[0][2] == field[1][2] && field[1][2] == field[2][2]) {
            System.out.println("Победитель: " + field[0][2]);
            return true;
        }

        int count = 0;
        for (Symbol[] symbols : field) {
            for (Symbol symbol : symbols) {
                if (symbol != null) {
                    count++;
                }
            }
        }

        return count == 9;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Symbol[] symbols : field) {
            for (Symbol symbol : symbols) {
                String s = symbol == null ? "_" : symbol + "";
                sb.append(s);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}

